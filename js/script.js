const slides = document.querySelectorAll('.slider .slider__item')
const sliderTrack = document.querySelector('.slider__track')
const sliderNextBtn = document.querySelector('.slider__nav-next')
const sliderPrevBtn = document.querySelector('.slider__nav-prev')
const dots = Object.assign(document.createElement('div'), {className: 'slider__dots'});
const dot = Object.assign(document.createElement('span'), {className: 'slider__dot'});

const sliderDots = document.getElementsByClassName('slider__dot');
sliderTrack.after(dots);
for (let i = 0; i < slides.length; i++) {
	let newDot = dot.cloneNode(true)
	newDot.setAttribute('data-id', i)
	dots.appendChild(newDot);
}
let count = 0
let width;

const init = (i) => {
	width = document.querySelector('.slider').offsetWidth
	sliderTrack.style.width = width * slides.length + 'px'
	for (let i = 0; i < slides.length; i++) {
		slides[i].style.width = width + 'px'
		slides[i].style.height = 'auto'
	}
	rollSlider(count)
}
const nextSlide = () => {
	slides[count].classList.remove('is-active')
	sliderDots[count].classList.remove('is-active')
	count++
	if (count >= slides.length) {
		count = 0
	}
	rollSlider(count)
}
const prevSlide = () => {
	slides[count].classList.remove('is-active')
	sliderDots[count].classList.remove('is-active')
	count--
	if (count < 0) count = slides.length - 1
	rollSlider(count)
}
const rollSlider = (i) => {
	slides[i].classList.add('is-active')
	sliderDots[i].classList.add('is-active')
	sliderTrack.style.transform = 'translate(-' + count * width + 'px)'
}

if (document.querySelector('.slider').dataset.autoplay) {
	setInterval(nextSlide, document.querySelector('.slider').dataset.autoplay);
}

window.addEventListener('resize', init)
sliderNextBtn.addEventListener('click', nextSlide)
sliderPrevBtn.addEventListener('click', prevSlide)

init()